lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(SbtWeb)
  .settings(
    name := "crudakka",
    version := "0.1",
    scalaVersion := "2.11.8",
    resolvers ++= Seq(
      "sonatype releases" at "http://oss.sonatype.org/content/repositories/releases",
      "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
    ),
    // https://github.com/sbt/sbt/issues/2217
    fullResolvers ~= { _.filterNot(_.name == "jcenter") },
    libraryDependencies ++= Seq(
      "org.scalikejdbc"      %% "scalikejdbc"                   % scalikejdbcVersion,
      "org.scalikejdbc"      %% "scalikejdbc-config"            % scalikejdbcVersion,
      "org.scalikejdbc"      %% "scalikejdbc-play-initializer"  % scalikejdbcPlayVersion,
      "org.scalikejdbc"      %% "scalikejdbc-play-fixture"      % scalikejdbcPlayVersion,
      "org.postgresql"       %  "postgresql"                    % "9.4-1206-jdbc41",
      "org.json4s"           %% "json4s-ext"                    % "3.3.+",
      "com.github.tototoshi" %% "play-json4s-native"            % "0.5.+",
      "org.flywaydb"         %% "flyway-play"                   % "3.0.+",
      "org.scalikejdbc"      %% "scalikejdbc-test"              % scalikejdbcVersion  % "test",
      "com.typesafe.akka"    %% "akka-actor"                    % "2.5.1",
      specs2 % "test"
    ),
    routesGenerator := InjectedRoutesGenerator,
    scalikejdbcSettings // http://scalikejdbc.org/documentation/setup.html
  ).settings(scalariformSettings)

lazy val scalikejdbcVersion = scalikejdbc.ScalikejdbcBuildInfo.version
lazy val scalikejdbcPlayVersion = "2.5.+"