package models

import org.joda.time.DateTime
import scalikejdbc.{ DBSession, SyntaxProvider, WrappedResultSet }
import play.api.libs.json._
import scalikejdbc._

/**
 * Created by musta on 2017-05-20.
 */
case class Phone(
    id: Option[Long],
    name: String,
    phone: String,
    createdAt: Option[DateTime]
) {

  def save()(implicit session: DBSession = Phone.autoSession): Phone = Phone.save(this)(session)
  def delete()(implicit session: DBSession = Phone.autoSession): Boolean = id.exists(Phone.remove)
}

object Phone extends GenericDAO[Phone] {

  def findByName(name: String): Any = {
    println(s"ASD$name ASD")
    findBy(sqls"name = {name}", 'name -> name)
  }

  val p = Phone.syntax("p")

  override def construct(e: scalikejdbc.SyntaxProvider[Phone])(rs: WrappedResultSet): Phone = {
    Phone(
      rs.get(e.resultName.id),
      rs.get(e.resultName.name),
      rs.get(e.resultName.phone),
      rs.get(e.resultName.createdAt)
    )
  }

  override val entity: scalikejdbc.SQLSyntaxSupport[Phone] = Phone

  override val me: scalikejdbc.QuerySQLSyntaxProvider[scalikejdbc.SQLSyntaxSupport[Phone], Phone] = p

  def create(phone: Phone): Phone = {
    create(
      'name -> phone.name,
      'phone -> phone.phone,
      'created_at -> DateTime.now()
    )
  }

  def save(phone: Phone)(implicit session: DBSession): Phone = save(
    phone.id.get,
    'name -> phone.name,
    'phone -> phone.phone
  )

}
