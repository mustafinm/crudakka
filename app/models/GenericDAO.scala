package models

import scalikejdbc._

trait GenericDAO[T] extends SQLSyntaxSupport[T] {

  def construct(e: SyntaxProvider[T])(rs: WrappedResultSet): T

  val entity: SQLSyntaxSupport[T]
  val me: QuerySQLSyntaxProvider[SQLSyntaxSupport[T], T]
  val mappings: (WrappedResultSet => T) = construct(me)
  val whereClause: Option[SQLSyntax] = None
  val orderByColumns: Seq[SQLSyntax] = Nil

  def apply(sp: SyntaxProvider[T])(rs: WrappedResultSet): T = apply(sp.resultName)(rs)

  def apply(rn: ResultName[T])(rs: WrappedResultSet): T = construct(me)(rs)

  private def columnsAndValues(params: (Symbol, Any)*) = params.map(p => SQLSyntax.createUnsafely(p._1.name) -> AsIsParameterBinder(p._2))

  def buildSelectSql(count: Boolean = false) =
    if (count)
      select(sqls.count).from(entity as me)
    else
      select.from[T](entity as me)

  def findAll(offset: Option[Int], limit: Option[Int], asc: Option[String], desc: Option[String])(implicit session: DBSession = autoSession): List[T] = {
    val query = buildSelectSql().where(whereClause)
    val offsetQ = offset.map { o => query.offset(o) }.getOrElse(query)
    val limitQ = limit.map { l => offsetQ.limit(l) }.getOrElse(query)

    val descQ = desc.map { s => limitQ.orderBy(me.resultName.selectDynamic(s)).desc }.getOrElse(query)
    val ascQ = asc.map { s => limitQ.orderBy(me.resultName.selectDynamic(s)).asc }.getOrElse(descQ)

    val sql = withSQL {
      ascQ
        .orderBy(orderByColumns: _*)
    }
    sql.map(mappings).list().apply()
  }

  def findBy(where: SQLSyntax, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): List[T] = {
    val sql = withSQL {
      buildSelectSql().where.append(where).and(whereClause)
    }
    sql.bindByName(params: _*).map(mappings).list().apply()
  }

  def find(id: Long)(implicit session: DBSession = autoSession): Option[T] = {
    val sql = withSQL {
      buildSelectSql().where.eq(me.id, id).and(whereClause)
    }
    sql.map(mappings).single().apply()
  }

  def countAll()(implicit session: DBSession = autoSession): Long = {
    withSQL {
      buildSelectSql(true).where(whereClause)
    }.map(rs => rs.long(1)).single().apply().get
  }

  def countBy(where: SQLSyntax, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): Long = {
    val sql = withSQL {
      buildSelectSql(true).where.append(where).and(whereClause)
    }
    sql.bindByName(params: _*).map(rs => rs.long(1)).single().apply().get
  }

  def create(params: (Symbol, Any)*)(implicit session: DBSession = autoSession): T = {
    val id = withSQL {
      insert.into(entity)
        .namedValues(columnsAndValues(params: _*): _*)
    }.updateAndReturnGeneratedKey.apply()
    find(id).get
  }

  def save(id: Long, params: (Symbol, Any)*)(implicit session: DBSession = autoSession): T = {
    withSQL {
      update(entity).set(columnsAndValues(params: _*): _*).where.eq(sqls"id", id)
    }.update().apply()
    find(id).get
  }

  def remove(id: Long)(implicit session: DBSession = autoSession): Boolean = {
    find(id) match {
      case Some(e) => {
        withSQL {
          delete.from(entity as me)
            .where.eq(me.id, id)
        }.update().apply()
        true
      }
      case _ => false
    }
  }

}