package controllers

import javax.inject.{ Inject, Singleton }

import actor.PhoneActor
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import com.github.tototoshi.play2.json4s.native.Json4s
import models.Phone
import org.json4s._
import org.json4s.ext.JodaTimeSerializers
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.{ Action, Controller }

import scala.concurrent.duration._

/**
 * Created by musta on 2017-05-20.
 */
@Singleton
class PhonesController @Inject() (json4s: Json4s, system: ActorSystem) extends Controller {

  import PhoneActor._
  import json4s._

  implicit val timeout: Timeout = 10.seconds
  implicit val formats: Formats = DefaultFormats ++ JodaTimeSerializers.all

  val phoneActor = system.actorOf(PhoneActor.props, "phone-actor")

  def create = Action.async(json) { req =>
    val phoneReq = req.body.extract[Phone]
    (phoneActor ? Create(phoneReq)).mapTo[Phone].map { phone =>
      Created(Extraction.decompose(phone)).withHeaders(LOCATION -> s"/phones/${phone.id}")
    }
  }

  def find(name: String) = Action.async {
    (phoneActor ? Find(name)).mapTo[List[Phone]].map { phones =>
      Ok(Extraction.decompose(phones))
    }
  }

  def findAll(offset: Option[Int], limit: Option[Int], asc: Option[String], desc: Option[String]) = Action.async {
    (phoneActor ? FindAll(offset, limit, asc, desc)).mapTo[List[Phone]].map { phones =>
      Ok(Extraction.decompose(phones))
    }
  }

  def update(id: Long) = Action.async(json) { req =>
    val phone = req.body.extract[Phone].copy(id = Some(id))
    (phoneActor ? Update(phone)).mapTo[Option[Phone]].map {
      case Some(ph) => Ok(Extraction.decompose(ph))
      case None => NotFound
    }
  }

  def delete(id: Long) = Action.async {
    (phoneActor ? Delete(id)).mapTo[Boolean].map { deleted =>
      if (deleted) NoContent
      else NotFound
    }
  }

}
