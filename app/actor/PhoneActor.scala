package actor

import akka.actor.{ Actor, Props }
import models.Phone

/**
 * Created by musta on 2017-05-20.
 */
class PhoneActor extends Actor {

  import PhoneActor._

  def receive: Receive = {
    case Create(phone) => sender() ! Phone.create(phone)
    case Find(name) => sender() ! Phone.findByName(name)
    case FindAll(offset, limit, asc, desc) => sender() ! Phone.findAll(offset, limit, asc, desc)
    case Update(phone) =>
      sender() ! phone.id.flatMap { id =>
        Phone.find(id).map(_ => phone.save())
      }

    case Delete(id) =>
      sender() ! Phone.find(id).exists(_.delete())
  }
}

object PhoneActor {

  def props = Props[PhoneActor]

  case class Create(phone: Phone)

  case class Find(name: String)

  case class FindAll(offset: Option[Int], limit: Option[Int], asc: Option[String], desc: Option[String])

  case class Update(phone: Phone)

  case class Delete(id: Long)

}
