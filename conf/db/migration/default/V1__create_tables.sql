DROP TABLE IF EXISTS phone;
CREATE SEQUENCE phone_id_seq START WITH 1;
CREATE TABLE phone (
  id         BIGINT       NOT NULL DEFAULT nextval('phone_id_seq') PRIMARY KEY,
  name       VARCHAR(255) NOT NULL,
  phone      VARCHAR(255),
  created_at TIMESTAMP    NOT NULL
);

INSERT INTO phone (name, phone, created_at) VALUES ('Murat Mustafin', '+7 234 452 23 43', current_timestamp);
INSERT INTO phone (name, phone, created_at) VALUES ('Nurlan Nurlanov', '+7 701 231 42 99', current_timestamp);
INSERT INTO phone (name, phone, created_at) VALUES ('Vasya Pupkin', '+7 708 421 24 11', current_timestamp);
INSERT INTO phone (name, phone, created_at) VALUES ('John Doe', '+7 701 242 00 23', current_timestamp);
